# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pract06/version'

Gem::Specification.new do |spec|
  spec.name          = "pract06"
  spec.version       = Pract08::VERSION #nueva version de la practica 6
  spec.authors       = ["Mayra Garcia Cortes"]
  spec.email         = ["alu0100831818@ull.edu.es"]

  spec.summary       = %q{Practica 8, clases de ruby}
  spec.description   = %q{Se va a implementar una serie de clases tipo de dietas y clases referente al tipo de persona que va a utilizar la dieta}
  spec.homepage      = "https://alu0100831818@bitbucket.org/alu0100831818/pract08.git"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "guard-bundler"

end
